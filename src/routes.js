import MainPage from '@/components/pages/MainPage'
import ProgramPage from '@/components/pages/ProgramPage'
import BusketPage from '@/components/pages/BusketPage'

const ROUTE_NAME_MAIN = 'main'
const ROUTE_NAME_BUSKET = 'busket'
const ROUTE_NAME_PROGRAM = 'program'

export default [
  { name: ROUTE_NAME_MAIN, path: '/', component: MainPage },
  { name: ROUTE_NAME_BUSKET, path: '/busket', component: BusketPage },
  { name: ROUTE_NAME_PROGRAM, path: '/programs/:id', component: ProgramPage },
]

export {
  ROUTE_NAME_MAIN,
  ROUTE_NAME_BUSKET,
  ROUTE_NAME_PROGRAM,
}
