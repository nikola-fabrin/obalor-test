import axios from 'axios'

const adapter = axios.create({
    baseURL: 'https://run.mocky.io/v3',
})

adapter.interceptors.response.use(
    response => response,
    function (error) {
        console.error(error)
        return Promise.reject(error)
    }
)

export default adapter
