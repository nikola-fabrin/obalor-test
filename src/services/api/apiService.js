import adapter from '@/services/api/adapter'
import ProgramCategory from '@/models/ProgramCategory'
import Program from '@/models/Program';

const apiPaths = {
    programCategories: '/28603966-8132-4a7b-bfd0-b40aaa90be61',
    programs: '/96e9312c-0b63-4a84-b400-141c840789ee',
}

class ApiService {
    /** @type {AxiosInstance} */
    #adapter

    /**
     * @param {AxiosInstance} adapter
     */
    constructor (adapter) {
        this.#adapter = adapter
    }

    /**
     * Возвращает категории программ медицинского образования.
     *
     * @returns {Promise<ProgramCategory[],Error>}
     */
    async getProgramCategories () {
        return (await this.#adapter.get(apiPaths.programCategories)).data
            .map(item => new ProgramCategory(item))
    }

    /**
     * Возвращает программы медицинского образования.
     *
     * @param {number|null} categoryId Идентификатор категории программ.
     * @param {string} search Строка поиска по названию и описанию программ.
     * @param {number} limit Максимальное количество программ в результате.
     * @param {number} offset Смещение от начала списка программ.
     * @returns {Promise<Program[],Error>}
     */
    async getPrograms (categoryId = null, search = '', limit = 10, offset = 0) {
        const { data } = (await this.#adapter.get(apiPaths.programs, {
            params: { categoryId, search, limit, offset, }
        }))

        // Эмуляция фильтрации результатов на стороне реального API бэкенда.
        let items = data.items
        if (categoryId !== null) {
            items = items.filter(item => item.categoryIds.includes(categoryId))
        }

        search = search.trim()
        if (search) {
            // Разумеется, в реальном приложении поиск будет выполняться с помощью поискового движка.
            let searchRegExp = new RegExp(search.replace(/\s+/, '|'), 'i')
            items = items.filter(item => (item.name + ' ' + item.description).search(searchRegExp) !== -1)
        }

        data.items = items.slice(offset, limit)
        data.limit = limit
        data.offset = offset
        // Конец кода эмуляции.

        data.items = data.items.map(item => new Program(item))

        return data
    }

    /**
     * Возвращает массив программ по идентификаторам.
     *
     * @param {number[]} ids
     * @returns {Promise<Program[],Error>}
     */
    async getProgramByIds (ids) {
        const { data } = (await this.#adapter.get(apiPaths.programs, {
            params: { ids: ids.join(','), }
        }))

        // Эмуляция поиска программ по идентификаторам на стороне реального API бэкенда.
        data.items = data.items.filter(({ id }) => ids.includes(id))
        // Конец кода эмуляции.

        return data.items.map(item => new Program(item))
    }

    /**
     * Возвращает программу по идентификатору.
     *
     * @param {number} id
     * @returns {Promise<Program,Error>}
     */
    async getProgramById (id) {
        const program = (await this.getProgramByIds([+id]))[0]

        if (!program) {
            throw new Error('Program not found.')
        }
        return program
    }
}

export default new ApiService(adapter)
