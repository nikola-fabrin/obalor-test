
class BusketStorage {
  #storageKeyBusketProgramIds = 'busketProgramIds'

  /**
   * @param {Object<string,Program>} busket
   */
  saveProgramIds (busket) {
    try {
      window.localStorage.setItem(
        this.#storageKeyBusketProgramIds,
        Object.keys(busket).join(',')
      )
    } catch (e) {
      console.warn(e)
    }
  }

  /**
   * @returns {number[]}
   */
  getProgramIds () {
    let busketProgramIds = []
    try {
      busketProgramIds = window.localStorage.getItem(this.#storageKeyBusketProgramIds)
      busketProgramIds = busketProgramIds
        ? busketProgramIds.split(',').map(id => +id)
        : []
    } catch (e) {
      console.warn(e)
    }
    return busketProgramIds
  }
}

export default new BusketStorage()
