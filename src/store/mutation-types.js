
export const ADD_TO_BUSKET = 'addToBusket'
export const REMOVE_FROM_BUSKET = 'removeFromBusket'
export const INIT_BUSKET = 'initBusket'
export const CLEAR_BUSKET = 'clearBusket'
export const SET_PROGRAM_CATEGORIES = 'setProgramCategories'
export const SET_SELECTED_PROGRAM_CATEGORY = 'setSelectedProgramCategory'
export const SET_SEARCH = 'setSearch'
