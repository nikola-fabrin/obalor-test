import Vue from 'vue'
import Vuex from 'vuex'
import {
  ADD_TO_BUSKET,
  INIT_BUSKET,
  CLEAR_BUSKET,
  REMOVE_FROM_BUSKET,
  SET_PROGRAM_CATEGORIES,
  SET_SELECTED_PROGRAM_CATEGORY,
  SET_SEARCH,
} from './mutation-types'
import {
  INIT_STORE,
  LOAD_PROGRAM_CATEGORIES,
} from './action-types'
import apiService from '@/services/api/apiService'
import busketStorage from '@/services/storage/busketStorage'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    /** @type {Object<string,Program>} */
    busket: {},

    /** @type {ProgramCategory[]} */
    programCategories: [],

    /** @type {ProgramCategory} */
    selectedProgramCategory: null,

    /** @type {string} */
    search: '',
  },

  mutations: {
    /**
     * @param {Object} state
     * @param {Program[]} programs
     */
    [INIT_BUSKET] (state, programs) {
      let busket = {}
      programs.forEach(program => (busket[program.id] = program))
      state.busket = busket
    },

    /**
     * @param {Object} state
     * @param {Program} program
     */
    [ADD_TO_BUSKET] (state, program) {
      Vue.set(state.busket, program.id, program)
      busketStorage.saveProgramIds(state.busket)
    },

    /**
     * @param {Object} state
     * @param {number} programId
     */
    [REMOVE_FROM_BUSKET] (state, programId) {
      let busket = { ...state.busket }
      delete busket[programId]
      state.busket = busket
      busketStorage.saveProgramIds(busket)
    },

    [CLEAR_BUSKET] (state) {
      state.busket = {}
      busketStorage.saveProgramIds({})
    },

    /**
     * @param {Object} state
     * @param {ProgramCategory[]} categories
     */
    [SET_PROGRAM_CATEGORIES] (state, categories) {
      state.programCategories = categories
    },

    [SET_SELECTED_PROGRAM_CATEGORY] (state, category) {
      state.selectedProgramCategory = category
    },

    [SET_SEARCH] (state, search) {
      state.search = search
    },
  },

  actions: {
    async [INIT_STORE] ({ commit, dispatch }) {
      await dispatch(LOAD_PROGRAM_CATEGORIES)

      let busketProgramIds = busketStorage.getProgramIds()
      if (busketProgramIds.length) {
        commit(INIT_BUSKET, await apiService.getProgramByIds(busketProgramIds))
      }
    },

    async [LOAD_PROGRAM_CATEGORIES] ({ commit }) {
      commit(SET_PROGRAM_CATEGORIES, await apiService.getProgramCategories())
    },
  },
})

export default store
