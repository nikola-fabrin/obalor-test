import AbstractModel from '@/models/AbstractModel'

class ProgramCategory extends AbstractModel {
    /** @type {number} */
    id

    /** @type {string} */
    name

    constructor (props) {
        super(props)
        Object.assign(this, props)
    }
}

export default ProgramCategory
