import AbstractModel from '@/models/AbstractModel'

class Program extends AbstractModel {
    /** @type {number} */
    id

    /** @type {string} */
    name

    /** @type {string} */
    description

    /** @type {number} */
    price

    /** @type {string} */
    imageUrl

    /** @type {number[]} */
    categoryIds

    constructor (props) {
        super(props)
        Object.assign(this, props)
    }

    /**
     * @param {Object} props
     * @throws {Error|TypeError}
     */
    validate (props) {
        super.validate(props)

        // @todo Валидация значений.
    }
}

export default Program
