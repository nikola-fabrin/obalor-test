
class AbstractModel {
    constructor (props) {
        this.validate(props)
    }

    /**
     * @param {Object} props
     * @throws {Error|TypeError}
     */
    validate (props) {
        if (typeof props !== 'object') {
            throw new TypeError('props is not an object.')
        }
    }
}

export default AbstractModel
